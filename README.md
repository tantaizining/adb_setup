# adb_setup

#### 介绍
The adb tools.
这里有新旧两种安装方式。旧版即为根目录下的几个文件，需要下载下来，双击exe进行下载SDK。
新版在latest edition文件夹中，里面有两个压缩包和一个安装说明。
下面仅介绍更为方便的新版安装说明。

#### 软件架构
软件架构说明


#### 安装教程

1.  去www.androiddevtools.cn底下下载两个对应自己电脑的压缩文件，一个是plantform tools，一个是Android SDK。在latest edition中有Windows的两个文件，Windows用户也可以直接下载这两个。
2.  下载好两个压缩包后，将两个压缩包解压到无中文路径的地址下，可以直接解压到当前文件夹。
3.  复制这两个解压后文件夹的位置，去我的电脑属性、高级系统设置、环境变量中，添加环境变量，两个文件夹均需添加。
4.  进入cmd，输入adb，若出现一大堆东西，则安装成功。

#### 使用说明

1.  另外详细的使用说明可以参见latest edition中的使用说明txt文件。
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
